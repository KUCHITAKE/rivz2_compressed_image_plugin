// src/compressed_image_panel.hpp

#ifndef RVIZ2_COMPRESSED_IMAGE_PLUGIN__COMPRESSED_IMAGE_PANEL_HPP_
#define RVIZ2_COMPRESSED_IMAGE_PLUGIN__COMPRESSED_IMAGE_PANEL_HPP_

#include <QLabel>
#include <QWidget>
#include <memory>
#include <rclcpp/rclcpp.hpp>
#include <rviz_common/message_filter_display.hpp>
#include <rviz_common/panel.hpp>
#include <rviz_common/properties/string_property.hpp>
#include <sensor_msgs/msg/compressed_image.hpp>

namespace Ogre {
class ManualObject;
}

namespace rviz2_compressed_image_plugin {

class CompressedImagePanel : public rviz_common::Panel {
  Q_OBJECT

 public:
  CompressedImagePanel(QWidget* parent = nullptr);
  ~CompressedImagePanel() override;

  void onInitialize() override;
  void processMessage(
      const sensor_msgs::msg::CompressedImage::ConstSharedPtr msg);

 private Q_SLOTS:
  void updateTopic();

 private:
  std::shared_ptr<rclcpp::Node> node_;
  QLabel* image_label_;
  rclcpp::Subscription<sensor_msgs::msg::CompressedImage>::SharedPtr
      subscription_;
  rviz_common::properties::StringProperty* topic_property_;
};

}  // namespace rviz2_compressed_image_plugin

#endif  // RVIZ2_COMPRESSED_IMAGE_PLUGIN__COMPRESSED_IMAGE_PANEL_HPP_