// src/compressed_image_panel.cpp

#include "compressed_image_panel.hpp"

#include <QVBoxLayout>
#include <memory>
#include <string>

#include "cv_bridge/cv_bridge.h"
#include "rclcpp/rclcpp.hpp"
#include "rviz_common/display_context.hpp"
#include "rviz_common/frame_manager_iface.hpp"
#include "rviz_common/logging.hpp"
#include "rviz_common/properties/float_property.hpp"
#include "rviz_common/properties/int_property.hpp"
#include "rviz_common/properties/ros_topic_property.hpp"
#include "rviz_common/render_panel.hpp"
#include "rviz_rendering/objects/point_cloud.hpp"
#include "sensor_msgs/msg/compressed_image.hpp"

namespace rviz2_compressed_image_plugin {

CompressedImagePanel::CompressedImagePanel(QWidget *parent) : Panel(parent) {}

CompressedImagePanel::~CompressedImagePanel() {}

void CompressedImagePanel::onInitialize() {
  // Initialize the QLabel
  image_label_ = new QLabel(this);

  QVBoxLayout* layout = new QVBoxLayout;
  layout->addWidget(image_label_);
  setLayout(layout);
  setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

  // Initialize a new ROS2 node
  rclcpp::NodeOptions node_options;
  node_options.start_parameter_event_publisher(false);
  node_ = std::make_shared<rclcpp::Node>("rviz2_compressed_image_node", "",
                                         node_options);

  // Create the topic property
  topic_property_ = new rviz_common::properties::StringProperty(
      QString("Topic"), QString("/default/compressed_image_topic"),
      QString("The topic on which to subscribe for compressed images."),
      nullptr, SLOT(CompressedImagePanel::updateTopic), this);

  // Set up the subscription
  updateTopic();
}

void CompressedImagePanel::updateTopic() {
  // Unsubscribe from the previous topic
  subscription_.reset();

  // Subscribe to the new topic
  auto callback = std::bind(&CompressedImagePanel::processMessage, this,
                            std::placeholders::_1);
  subscription_ = node_->create_subscription<sensor_msgs::msg::CompressedImage>(
      topic_property_->getStdString(), 10, callback);
}

void CompressedImagePanel::processMessage(
    const sensor_msgs::msg::CompressedImage::ConstSharedPtr msg) {
  try {
    // Convert the compressed image to a regular image
    cv_bridge::CvImagePtr cv_ptr = cv_bridge::toCvCopy(msg, "bgr8");

    // Create QImage and QPixmap from the converted OpenCV image
    QImage qimage(cv_ptr->image.data, cv_ptr->image.cols, cv_ptr->image.rows,
                  cv_ptr->image.step, QImage::Format_RGB888);
    QPixmap pixmap = QPixmap::fromImage(qimage.rgbSwapped());

    // Set the QPixmap to the QLabel
    image_label_->setPixmap(pixmap);
  } catch (const cv_bridge::Exception& e) {
    RVIZ_COMMON_LOG_ERROR_STREAM(
        "Error decoding compressed image: " << e.what());
  }
}

}  // namespace rviz2_compressed_image_plugin

#include "pluginlib/class_list_macros.hpp"

// Register the plugin with pluginlib.
PLUGINLIB_EXPORT_CLASS(rviz2_compressed_image_plugin::CompressedImagePanel,
                       rviz_common::Panel)